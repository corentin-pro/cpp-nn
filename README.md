# C++ Neural Network


## Description

This is a demonstration of neural network computing using the Eigen library. The project builds 3 executable :

  * **iris_graph** : neural network created from nodes only, training on iris dataset (https://archive.ics.uci.edu/ml/datasets/iris)
  * **iris** : same as iris_graph but using handy FCLayer structures to build the network
  * **mnist** : training a neural network on the MNIST dataset


## Why Eigen

The project aimed at building neural networks without any library, SIMD (same instruction multiple data) is very appealing when dealing with vector/matrix operation. SIMD can improve greatly performance on CPU, unfortunately this functionality has architecture-wise implementation. Eigen is a good wrapper to compile code for SIMD optimization, it is used by OpenCV, Tensorflow, Caffe, etc. Elso Eigen gives intersting Vector and Matrix structures that will make the code more readable.


## Dependencies

The project was made to compile on Linux, the needed packages are :

  * **build-essential** : to use `make` and `g++` (g++ 6+ only, Ubuntu 16 will not work)
  * **libeigen3-dev** : to include the Eigen library in the code (can be downloaded instead from the [official website](http://eigen.tuxfamily.org/index.php?title=Main_Page#Download)). On arch the package is named `eigen`

The following should already be installed by default on most linux distribution:
  * **pkg-config** : used in the makefile to help including eigen (can be done manually specifying `-I/path/to/eigen`)
  * **wget** : only necessary if you want to download MNIST for the `download_mnist.sh` script

On Debian/Ubuntu distributions they can ba intalled with the following :

```sudo apt install build-essential libeigen3-dev pkg-config wget```


## Usage

Building the programs only requires to use command :

```make```

To run the MNIST executable, the MNIST files has to be downloaded in the same directory as the executable file. A `download_mnist.sh` file can be use to do this task :

```./download_mnist```

There are 3 different executables to run `iris_graph`, `iris` and `mnist`. Using the parameter `--help` (or making any error in the parameter) will show a simple usage message.


## Comments

In this project `test` data designates the data randomly split from the training data and `validation` designate data aside from the train/test split that is used only after the training to validate the test accuracy.

Iris dataset is very easy to train, deep learning is not really necessary for such a task. 100% accuracy can be achieve easily but the lack of data makes such a number very inaccuarate of the really network capability.

The MNIST example is more challenging, the *Test set* is used here as validation, although usually validation would be better if taken from an other *domain*, MNIST data being only 28x28 greyscale images doesn't allow a good diversity in the data anyway.

Note that the MNIST loader already normalize the image from 0-255 (integer) to 0-1 (float). This could be done by a specific node in that graph but would slow the training (can be done once at loading time).

Relu activation is implemented but for the 2 simple dataset given the Sigmoid activation is enough (not a lot of abstraction to be made). It is harder to train networks with Relu activations and would be better with a proper Xavier initialization.

Due to the lack of time several intersting implementation has been skipped (for now) :
  * batch normalization : would help the training for deeper networks, should be the next step
  * Xavier initialization : would help all network to train better
  * convolution layer : would allow proper training on image for dataset like CIFAR
  * dropout : easy implementation that help to fight overfitting
