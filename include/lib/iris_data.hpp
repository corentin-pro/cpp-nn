#pragma once

#include <memory>
#include <vector>

#include <Eigen/Core>


namespace IrisData
{
    using namespace std;

    vector<shared_ptr<Eigen::MatrixXf>> LoadData(const char*);
    vector<shared_ptr<Eigen::MatrixXf>> LoadLabel(const char*);
}
