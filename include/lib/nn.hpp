#pragma once

#include <vector>
#include <memory>
#include <unordered_map>

#include <Eigen/Core>

namespace CppNN
{
    using namespace std;
    class Placeholder;
    class Variable;
    class AddNode;
    class MulNode;
    using NetworkFeed = unordered_map<Placeholder*, shared_ptr<Eigen::MatrixXf>>;

    class Node: public enable_shared_from_this<Node>
    {
        public:
            vector<shared_ptr<Node>> parents;
            shared_ptr<Node> child = nullptr; // simple grpah for now, this can be changed to vector latter

            string name = "Node";

            Eigen::MatrixXf output;
            Eigen::MatrixXf gradient;

            virtual Eigen::MatrixXf& Infer(const NetworkFeed&) { return this->output; };
            virtual void BackProp(Eigen::MatrixXf& gradient) { this->gradient = gradient; };
            virtual void ApplyGradient(const float&);
            virtual void ApplyAccumulatedGradient();
            virtual void AccumulateGradient(const float&, const unsigned int&);

            shared_ptr<AddNode> operator+(shared_ptr<Node>);
            shared_ptr<MulNode> operator*(shared_ptr<Node>);
    };

    class Placeholder: public Node
    {
        public:
            shared_ptr<Eigen::MatrixXf> input;

            Placeholder(const unsigned int&, const unsigned int&, const string& = "Placeholder");

            Eigen::MatrixXf& Infer(const NetworkFeed&) override;
    };

    class Variable: public Node
    {
        public:
            Eigen::MatrixXf value;

            Eigen::MatrixXf gradient_accumulator;

            Variable(const unsigned int&, const unsigned int&, const string& = "Variable");

            Eigen::MatrixXf& Infer(const NetworkFeed&) override;
            void ApplyGradient(const float&) override;
            virtual void ApplyAccumulatedGradient() override;
            virtual void AccumulateGradient(const float&, const unsigned int&) override;
    };

    class OpNode: public Node
    {
        public:
            shared_ptr<Node> input_1;
            shared_ptr<Node> input_2;

            OpNode(shared_ptr<Node>, shared_ptr<Node>, const string& = "OpNode");
            OpNode(const Node*, shared_ptr<Node>&);
    };

    class AddNode: public OpNode
    {
        public:
            AddNode(shared_ptr<Node> input_1, shared_ptr<Node> input_2, const string& name = "AddNode")
                :OpNode(input_1, input_2, name) {};
            Eigen::MatrixXf& Infer(const NetworkFeed&) override;
            void BackProp(Eigen::MatrixXf&) override;
    };

    class MulNode: public OpNode
    {
        public:
            MulNode(shared_ptr<Node> input_1, shared_ptr<Node> input_2, const string& name = "MulNode")
                :OpNode(input_1, input_2, name) {};
            Eigen::MatrixXf& Infer(const NetworkFeed&) override;
            void BackProp(Eigen::MatrixXf&) override;
    };

    class SigmoidNode: public Node
    {
        public:
            shared_ptr<Node> input;

            SigmoidNode(shared_ptr<Node>, const string& = "SigmoidNode");
            Eigen::MatrixXf& Infer(const NetworkFeed&) override;
            void BackProp(Eigen::MatrixXf&) override;
    };

    class ReluNode: public Node
    {
        public:
            shared_ptr<Node> input;

            ReluNode(shared_ptr<Node>, const string& = "ReluNode");
            Eigen::MatrixXf& Infer(const NetworkFeed&) override;
            void BackProp(Eigen::MatrixXf&) override;
    };

    enum class VariableInitializer
    {
        None, Zero, Random
    };

    enum class Activation
    {
        None, Sigmoid, Relu
    };

    class FCLayer
    {
        public:
            string name = "FCLayer";
            shared_ptr<Variable> weight;
            shared_ptr<Variable> bias;
            shared_ptr<MulNode> mul_node;
            shared_ptr<AddNode> add_node;
            shared_ptr<Node> activation_node;
            shared_ptr<Node> output;

            FCLayer(
                shared_ptr<Node>,
                const unsigned int&,
                const unsigned int&,
                const Activation& = Activation::Relu,
                const VariableInitializer& = VariableInitializer::Random);
    };
}
