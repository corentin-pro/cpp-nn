#pragma once

#include <memory>
#include <vector>

#include <Eigen/Core>

#include "lib/nn.hpp"


namespace Utils
{
    using namespace std;
    using namespace CppNN;

    using MatrixList = vector<shared_ptr<Eigen::MatrixXf>>;
    using DataTuple = tuple<shared_ptr<Eigen::MatrixXf>, shared_ptr<Eigen::MatrixXf>>;

    vector<DataTuple> BundleDataLabel(const MatrixList&, const MatrixList&);
    tuple<vector<DataTuple>, vector<DataTuple>> SplitData(
        const MatrixList&, const MatrixList&, const float = 0.9, const unsigned int = 0);
    float TestAccuracy(
        const vector<DataTuple>&,
        NetworkFeed,
        shared_ptr<Placeholder>,
        shared_ptr<Node>);
    void PrintGraph(shared_ptr<Node>);
}
