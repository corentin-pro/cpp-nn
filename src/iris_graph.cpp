#include <iostream>

#include <Eigen/Core>

#include "lib/iris_data.hpp"
#include "lib/nn.hpp"
#include "lib/utils.hpp"


using namespace std;
using namespace Eigen;
using namespace CppNN;


int main(int argc, char* argv[])
{
    // Usage check
    try
    {
        if(argc > 1) stof(argv[1]);
        if(argc > 2) stoi(argv[2]);
        if(argc > 3) stoi(argv[3]);
        if(argc > 4) stoi(argv[4]);
    }
    catch(const exception& exception)
    {
        cout << "Usage: iris_graph [LEARNING_RATE] [BATCH_SIZE] [MAX_EPOCH] [MAX_TRAIN_OUTPUT]" << endl;
        return 0;
    }

    // -- Parameters --
    cout << "--- Parameters ---" << endl;
    const float learning_rate           = (argc > 1) ? stof(argv[1]) : 0.1;
    const unsigned int batch_size       = (argc > 2) ? stoi(argv[2]) : 1;
    const unsigned int max_epoch        = (argc > 3) ? stoi(argv[3]) : 10;
    const unsigned int max_train_output = (argc > 4) ? stoi(argv[4]) : 10;

    cout << "learning_rate : " << learning_rate << endl;
    cout << "batch_size : " << batch_size << endl;
    cout << "max_epoch : " << max_epoch << endl;
    cout << "max_train_output : " << max_train_output << endl;

    // --- Data Loading ---
    cout << endl << "--- Data ---" << endl;
    auto iris_data = IrisData::LoadData("bezdekIris.data");
    auto iris_label = IrisData::LoadLabel("bezdekIris.data");
    const unsigned int data_size = iris_data.size();

    if(!data_size)
    {
        cout << "No data loaded" << endl;
        return 0;
    }

    const unsigned int in_dimension = iris_data[0]->cols();
    const unsigned int out_classes = iris_label[0]->cols();

    auto data_split = Utils::SplitData(iris_data, iris_label);
    auto train_split = get<0>(data_split);
    auto test_split = get<1>(data_split);
    const unsigned int train_size = train_split.size();
    const unsigned int test_size = test_split.size();

    cout << "Data : " << data_size << " samples -> " << train_size << " train + " << test_size << " test" << endl;
 
    // --- Network ---
    cout << endl << "--- Network ---" << endl;
    auto symbolic_input = make_shared<Placeholder>(1, in_dimension);

    auto weight1 = make_shared<Variable>(in_dimension, 10, "weight1");
    weight1->value = Eigen::MatrixXf::Random(in_dimension, 10);
    auto mul1 = *symbolic_input * weight1;
    auto biases1 = make_shared<Variable>(1, 10, "biases1");
    auto add1 = *mul1 + biases1;
    auto sigmoid1 = make_shared<SigmoidNode>(add1, "sigmoid1");

    auto weight2 = make_shared<Variable>(10, out_classes, "weight2");
    weight2->value = Eigen::MatrixXf::Random(10, out_classes);
    auto mul2 = *sigmoid1 * weight2;
    auto biases2 = make_shared<Variable>(1, out_classes, "biases2");
    auto add2 = *mul2 + biases2;
    auto sigmoid2 = make_shared<SigmoidNode>(add2, "sigmoid2");

    auto network_output = sigmoid2;
    NetworkFeed feed = {{symbolic_input.get(), nullptr}};

    Utils::PrintGraph(network_output);

    // --- Test ---
    cout << endl << "--- Test ---" << endl;
    cout << "Test accuracy : " << Utils::TestAccuracy(test_split, feed, symbolic_input, network_output) << "%" << endl;

    // --- Train ---
    cout << endl << "--- Train ---" << endl;
    auto loss_gradient = MatrixXf(1, out_classes);
    unsigned int batch_step = 0;
    for(unsigned int epoch = 0 ; epoch < max_epoch ; epoch += 1)
    {
        for(auto& data_tuple: train_split)
        {
            // Infer and calculate loss gradient (using derivate of mean square wich is just a substraction here)
            feed[symbolic_input.get()] = get<0>(data_tuple);
            loss_gradient.noalias() = network_output->Infer(feed) - *(get<1>(data_tuple));
            // Calculate gradients
            network_output->BackProp(loss_gradient);
            // Update variables
            batch_step += 1;
            if(batch_size == 1)
            {
                network_output->ApplyGradient(learning_rate);
            }
            else
            {
                network_output->AccumulateGradient(learning_rate, batch_size);
                if(batch_step >= (batch_size - 1))
                {
                    network_output->ApplyAccumulatedGradient();
                    batch_step = 0;
                }
            }
        }

        if((epoch % (unsigned int)(max_epoch / max_train_output)) == 0)
            cout << "End of epoch " << epoch
                << " : test accuracy " << Utils::TestAccuracy(test_split, feed, symbolic_input, network_output) << "%" << endl;
    }
    cout << "Training over" << endl;
    cout << "End of epoch " << max_epoch - 1
            << " : test accuracy " << Utils::TestAccuracy(test_split, feed, symbolic_input, network_output) << "%" << endl;

    // --- Test ---
    cout << endl << "--- Final test ---" << endl;
    cout << "Test accuracy : " << Utils::TestAccuracy(test_split, feed, symbolic_input, network_output) << "%" << endl;

    cout << "Done" << endl;
    return 0;
}
