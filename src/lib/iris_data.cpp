#include "lib/iris_data.hpp"

#include <fstream>
#include <string>


using namespace std;

vector<shared_ptr<Eigen::MatrixXf>> IrisData::LoadData(const char* file_name)
{
    vector<shared_ptr<Eigen::MatrixXf>> result;

    ifstream data_file;
    data_file.open(file_name);
    if(data_file.is_open())
    {
        string line;
        while(getline(data_file, line))
        {
            if(line.size() > 10)
            {
                auto new_data = make_shared<Eigen::MatrixXf>(1, 4);
                for(int data_index = 0 ; data_index < 4 ; data_index++)
                    (*new_data)(0, data_index) = stof(line.substr(4 * data_index, 3).c_str());
                result.push_back(new_data);
            }
        }
        data_file.close();
    }

    return result;
}

vector<shared_ptr<Eigen::MatrixXf>> IrisData::LoadLabel(const char* file_name)
{
    vector<shared_ptr<Eigen::MatrixXf>> result;

    ifstream data_file;
    data_file.open(file_name);
    if(data_file.is_open())
    {
        string line;
        while(getline(data_file, line))
        {
            if(line.size() > 10)
            {
                auto new_data = make_shared<Eigen::MatrixXf>(1, 3);
                new_data->setZero(1, 3);
                auto iris_name = line.substr(21, 3);
                if(iris_name == "set")
                    (*new_data)(0, 0) = 1;
                else if(iris_name == "ver")
                    (*new_data)(0, 1) = 1;
                else
                    (*new_data)(0, 2) = 1;
                result.push_back(new_data);
            }
        }
        data_file.close();
    }

    return result;
}
