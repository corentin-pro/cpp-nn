#include "lib/mnist_data.hpp"

#include <iostream>
#include <fstream>
#include <string>

// Avoiding any endian problem
#define MEM_TO_INT32(input) (static_cast<unsigned char>(input[0]) << 24) \
 + (static_cast<unsigned char>(input[1]) << 16) \
 + (static_cast<unsigned char>(input[2]) << 8) \
 + static_cast<unsigned char>(input[3])


using namespace std;

vector<shared_ptr<Eigen::MatrixXf>> MnistData::LoadData(const char* file_name)
{
    vector<shared_ptr<Eigen::MatrixXf>> result;

    cout << "Opening : " << file_name << endl;
    ifstream data_file;
    data_file.open(file_name, ios::out | ios::binary);
    if(data_file.is_open())
    {
        int32_t magic_number;
        unsigned long image_count;
        int32_t row_number;
        int32_t column_number;
        char memblock[4];
        data_file.seekg(0, ios::beg);
        data_file.read(memblock, 4);
        magic_number = MEM_TO_INT32(memblock);
        cout << "Magic number : " << magic_number << endl;

        data_file.read(memblock, 4);
        image_count = MEM_TO_INT32(memblock);
        cout << "Data size : " << image_count << endl;

        data_file.read(memblock, 4);
        row_number = MEM_TO_INT32(memblock);
        cout << "Row number : " << row_number << endl;

        data_file.read(memblock, 4);
        column_number = MEM_TO_INT32(memblock);
        cout << "Column number : " << column_number << endl;

        char buffer;
        const int input_size = row_number * column_number;
        while(data_file.get(buffer))
        {
            auto new_data = make_shared<Eigen::MatrixXf>(1, input_size);
            (*new_data)(0, 0) = (float)(static_cast<unsigned char>(buffer)) / 255;
            for(int data_index = 1 ; data_index < input_size ; data_index++)
            {
                (*new_data)(0, data_index) = (float)(static_cast<unsigned char>(buffer)) / 255;
                data_file.get(buffer);
            }
            result.push_back(new_data);
        }
        data_file.close();
    }

    cout << endl;
    return result;
}

vector<shared_ptr<Eigen::MatrixXf>> MnistData::LoadLabel(const char* file_name)
{
    vector<shared_ptr<Eigen::MatrixXf>> result;

    cout << "Opening : " << file_name << endl;
    ifstream data_file;
    data_file.open(file_name, ios::out | ios::binary);
    if(data_file.is_open())
    {
        int32_t magic_number;
        unsigned long image_count;
        char memblock[4];

        data_file.seekg(0, ios::beg);

        data_file.read(memblock, 4);
        magic_number = MEM_TO_INT32(memblock);
        cout << "Magic number : " << magic_number << endl;

        data_file.read(memblock, 4);
        image_count = MEM_TO_INT32(memblock);
        cout << "Data size : " << image_count << endl;

        char buffer;
        constexpr int label_classes = 10;
        while(data_file.get(buffer))
        {
            auto new_data = make_shared<Eigen::MatrixXf>(1, label_classes);
            for(int data_index = 0 ; data_index < label_classes ; data_index++)
                (*new_data)(0, data_index) = (float)((int)(static_cast<unsigned char>(buffer)) == data_index);
            result.push_back(new_data);
        }
        data_file.close();
    }

    cout << endl;
    return result;
}
