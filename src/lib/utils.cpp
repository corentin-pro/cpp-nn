#include "lib/utils.hpp"

#include <algorithm>
#include <ctime>
#include <iostream>
#include <memory>
#include <vector>

using namespace Utils;
using namespace std;
using namespace CppNN;

vector<DataTuple> Utils::BundleDataLabel(const MatrixList& data, const MatrixList& label)
{
    vector<DataTuple> data_tuples;

    const unsigned int data_size = data.size();
    for(unsigned int index = 0 ; index < data_size ; index++)
        data_tuples.push_back({data[index], label[index]});

    return data_tuples;
}

tuple<vector<DataTuple>, vector<DataTuple>> Utils::SplitData(
    const MatrixList& data, const MatrixList& label, const float ratio, const unsigned int seed)
{
    vector<DataTuple> data_tuples;
    vector<DataTuple> train_tuples;
    vector<DataTuple> test_tuples;

    const unsigned int data_size = data.size();
    const unsigned int train_size = (unsigned int)((float)(data.size()) * ratio);

    for(unsigned int index = 0 ; index < data_size ; index++)
        data_tuples.push_back({data[index], label[index]});

    if(!seed)
        srand(time(nullptr));
    else
        srand(seed);
    random_shuffle(data_tuples.begin(), data_tuples.end());

    for(unsigned int index = 0 ; index < data_size ; index++)
    {
        if(index < train_size)
            train_tuples.push_back(data_tuples[index]);
        else
            test_tuples.push_back(data_tuples[index]);
    }

    return {train_tuples, test_tuples};
}

float Utils::TestAccuracy(
    const vector<DataTuple>& test_split,
    NetworkFeed feed,
    shared_ptr<Placeholder> symbolic_input,
    shared_ptr<Node> ouput_node)
{
    const unsigned int test_size = test_split.size();
    const unsigned int out_classes = get<1>(test_split[0])->cols();

    float test_accuracy_count = 0;
    for(auto& test_tuple : test_split)
    {
        feed[symbolic_input.get()] = get<0>(test_tuple);
        ouput_node->Infer(feed);
        float max_infer_value = 0;
        float max_infer_index = 0;
        for(unsigned int result_index = 0 ; result_index < out_classes ; result_index += 1)
        {
            if(ouput_node->output(0, result_index) > max_infer_value)
            {
                max_infer_value = ouput_node->output(0, result_index);
                max_infer_index = result_index;
            }
        }
        if((*(get<1>(test_tuple)))(0, max_infer_index) == 1)
            test_accuracy_count += 1;
    }
    return (test_accuracy_count / test_size) * 100;
}

void Utils::PrintGraph(shared_ptr<Node> output_node)
{
    vector<vector<shared_ptr<Node>>> graph;
    vector<tuple<unsigned int, shared_ptr<Node>>> node_queue;
    cout << endl;
    node_queue.push_back({0, output_node});

    while(!node_queue.empty())
    {
        auto graph_tuple = node_queue.back();
        node_queue.pop_back();
        auto current_depth = get<0>(graph_tuple);
        auto current_node = get<1>(graph_tuple);

        // Add current to graph
        if(graph.size() < current_depth + 1)
            graph.resize(current_depth + 1);
        graph[current_depth].push_back(current_node);

        // Queue parents to graph
        if(!current_node->parents.empty())
            for(auto parent_node: current_node->parents)
                node_queue.push_back({current_depth + 1, parent_node});
    }

    for(auto graph_depth_iterator = graph.rbegin() ; graph_depth_iterator != graph.rend() ; graph_depth_iterator++)
    {
        if(graph_depth_iterator != graph.rbegin())
        {
            auto previous_depth = graph_depth_iterator - 1;
            char link[2] = "|";
            for(auto node_iterator = previous_depth->rbegin() ; node_iterator != previous_depth->rend() ; node_iterator++)
            {
                const unsigned int name_length = (*node_iterator)->name.size();
                const unsigned int half_name_length = (*node_iterator)->name.size() / 2;
                for(unsigned int char_count = 0; char_count < name_length ; char_count += 1)
                {
                    if(char_count == half_name_length)
                    {
                        cout << link;
                        link[0] = '/';
                    }
                    else
                        cout  << " ";
                }
                cout  << " ";
            }
            cout  << endl;
        }
        for(auto node_iterator = graph_depth_iterator->rbegin() ; node_iterator != graph_depth_iterator->rend() ; node_iterator++)
            cout << (*node_iterator)->name << " ";
        cout << endl;
    }
}
