#include "lib/nn.hpp"

using namespace std;
using namespace CppNN;

// -------- Node --------

shared_ptr<AddNode> Node::operator+(shared_ptr<Node> input)
{
    return make_shared<AddNode>(shared_from_this(), input);
}


shared_ptr<MulNode> Node::operator*(shared_ptr<Node> input)
{
    return make_shared<MulNode>(shared_from_this(), input);
}

void Node::ApplyGradient(const float& learning_rate)
{
    if(!this->parents.empty())
        for(auto& parent : this->parents)
            parent->ApplyGradient(learning_rate);
}

void Node::ApplyAccumulatedGradient()
{
    if(!this->parents.empty())
        for(auto& parent : this->parents)
            parent->ApplyAccumulatedGradient();
}

void Node::AccumulateGradient(const float& learning_rate, const unsigned int& batch_size)
{
    if(!this->parents.empty())
        for(auto& parent : this->parents)
            parent->AccumulateGradient(learning_rate, batch_size);
}


// -------- Placeholder --------

Placeholder::Placeholder(const unsigned int& rows, const unsigned int& cols, const string& name)
{
    this->name = name;
    this->output.resize(rows, cols);
}


Eigen::MatrixXf& Placeholder::Infer(const NetworkFeed& network_feed)
{
    this->output = *(network_feed.at(this));
    return this->output;
}

// -------- Variable --------

Variable::Variable(const unsigned int& rows, const unsigned int& cols, const string& name)
{
    this->name = name;
    this->value.resize(rows, cols);
    this->gradient_accumulator.resize(rows, cols);
    this->gradient_accumulator.setZero(rows, cols);
}

Eigen::MatrixXf& Variable::Infer(const NetworkFeed&)
{
    this->output = this->value;
    return this->output;
}

void Variable::ApplyGradient(const float& learning_rate)
{
    this->value -= this->gradient * learning_rate;
    if(!this->parents.empty())
        for(auto& parent : this->parents)
            parent->ApplyGradient(learning_rate);
}

void Variable::ApplyAccumulatedGradient()
{
    this->value.noalias() -= this->gradient_accumulator;
    this->gradient_accumulator.setZero();
    if(!this->parents.empty())
        for(auto& parent : this->parents)
            parent->ApplyAccumulatedGradient();
}

void Variable::AccumulateGradient(const float& learning_rate, const unsigned int& batch_size)
{
    this->gradient_accumulator.noalias() += (this->gradient / batch_size) * learning_rate;
    if(!this->parents.empty())
        for(auto& parent : this->parents)
            parent->AccumulateGradient(learning_rate, batch_size);
}

// -------- OpNode --------

OpNode::OpNode(shared_ptr<Node> input_1, shared_ptr<Node> input_2, const string& name)
{
    // Allows the use of shared_from_this()
    auto wptr = shared_ptr<Node>( this, [](Node*){} );

    this->name = name;

    this->input_1 = input_1;
    this->input_2 = input_2;
    input_1->child = shared_from_this();
    input_2->child = shared_from_this();
    this->parents.push_back(input_1);
    this->parents.push_back(input_2);
}

// -------- AddNode --------

Eigen::MatrixXf& AddNode::Infer(const NetworkFeed& network_feed)
{
    this->output.noalias() = this->input_1->Infer(network_feed) + this->input_2->Infer(network_feed);
    return this->output;
}

void AddNode::BackProp(Eigen::MatrixXf& gradient)
{
    this->gradient = gradient;
    this->input_1->BackProp(this->gradient);
    this->input_2->BackProp(this->gradient);
}

// -------- MulNode --------

Eigen::MatrixXf& MulNode::Infer(const NetworkFeed& network_feed)
{
    // Matrix multiplication (not element-wise)
    this->output.noalias() = this->input_1->Infer(network_feed) * this->input_2->Infer(network_feed);
    return this->output;
}

void MulNode::BackProp(Eigen::MatrixXf& gradient)
{
    this->gradient = gradient;
    Eigen::MatrixXf gradient_1 = this->gradient * this->input_2->output.transpose();
    Eigen::MatrixXf gradient_2 = this->input_1->output.transpose() * this->gradient;
    this->input_1->BackProp(gradient_1);
    this->input_2->BackProp(gradient_2);
}

// -------- SigmoidNode --------

SigmoidNode::SigmoidNode(shared_ptr<Node> input, const string& name)
{
    // Allows the use of shared_from_this()
    auto wptr = shared_ptr<Node>( this, [](Node*){} );

    this->name = name;

    this->input = input;
    input->child = shared_from_this();
    this->parents.push_back(input);
}

// Macros applying to Array only (element wise operation)
#define SIGMOID(input) (1.0 / (1.0 + (-input).exp()))
#define SIGMOID_PRIME(input) (SIGMOID(input) * (1 - SIGMOID(input)))

Eigen::MatrixXf& SigmoidNode::Infer(const NetworkFeed& network_feed)
{
    this->output.noalias() = SIGMOID(this->input->Infer(network_feed).array()).matrix();
    return this->output;
}

void SigmoidNode::BackProp(Eigen::MatrixXf& gradient)
{
    const Eigen::ArrayXXf output_array = this->output.array();
    // (output_array * (1 - output_array)) is an optimization : sigmoid' = sigmoid * (1 - sigmoid)
    this->gradient.noalias() = (gradient.array() * (output_array * (1 - output_array))).matrix();
    this->input->BackProp(this->gradient);
}

// -------- ReluNode --------

ReluNode::ReluNode(shared_ptr<Node> input, const string& name)
{
    // Allows the use of shared_from_this()
    auto wptr = shared_ptr<Node>( this, [](Node*){} );

    this->name = name;

    this->input = input;
    input->child = shared_from_this();
    this->parents.push_back(input);
}

Eigen::MatrixXf& ReluNode::Infer(const NetworkFeed& network_feed)
{
    this->output.noalias() = this->input->Infer(network_feed).cwiseMax(0);
    return this->output;
}

void ReluNode::BackProp(Eigen::MatrixXf& gradient)
{
    this->gradient.noalias() = (gradient.array() * this->output.cwiseSign().array()).matrix();
    this->input->BackProp(this->gradient);
}

// -------- FCLayer --------

FCLayer::FCLayer(
    shared_ptr<Node> input_node,
    const unsigned int& in_dimension,
    const unsigned int& out_dimension,
    const Activation& activation,
    const VariableInitializer& var_init)
{
    this->weight = make_shared<Variable>(in_dimension, out_dimension, "weight");
    switch(var_init)
    {
        case VariableInitializer::Zero:
            this->weight->value = Eigen::MatrixXf::Zero(in_dimension, out_dimension);
            break;
        case VariableInitializer::Random:
            this->weight->value = Eigen::MatrixXf::Random(in_dimension, out_dimension);
            break;
        default:
            break;
    }
    this->mul_node = *input_node * this->weight;
    this->bias = make_shared<Variable>(1, out_dimension, "bias");
    this->add_node = *(this->mul_node) + this->bias;
    switch(activation)
    {
        case Activation::Sigmoid:
            this->activation_node = make_shared<SigmoidNode>(this->add_node);
            this->output = this->activation_node;
            break;
        case Activation::Relu:
            this->activation_node = make_shared<ReluNode>(this->add_node);
            this->output = this->activation_node;
            break;
        default:
            this->output = this->add_node;
            break;
    }
}

