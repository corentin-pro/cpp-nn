#include <iostream>
#include <string>

#include <Eigen/Core>

#include "lib/mnist_data.hpp"
#include "lib/nn.hpp"
#include "lib/utils.hpp"

using namespace std;
using namespace Eigen;
using namespace CppNN;


int main(int argc, char* argv[])
{
    // Usage check
    try
    {
        if(argc > 1) stof(argv[1]);
        if(argc > 2) stoi(argv[2]);
        if(argc > 3) stoi(argv[3]);
        if(argc > 4) stoi(argv[4]);
    }
    catch(const exception& exception)
    {
        cout << "Usage: mnist [LEARNING_RATE] [BATCH_SIZE] [MAX_EPOCH] [MAX_TRAIN_OUTPUT]" << endl;
        return 0;
    }

    // -- Parameters --
    cout << "--- Parameters ---" << endl;
    const float learning_rate           = (argc > 1) ? stof(argv[1]) : 0.1;
    const unsigned int batch_size       = (argc > 2) ? stoi(argv[2]) : 16;
    const unsigned int max_epoch        = (argc > 3) ? stoi(argv[3]) : 10;
    const unsigned int max_train_output = (argc > 4) ? stoi(argv[4]) : 10;

    cout << "learning_rate : " << learning_rate << endl;
    cout << "batch_size : " << batch_size << endl;
    cout << "max_epoch : " << max_epoch << endl;
    cout << "max_train_output : " << max_train_output << endl;

    // --- Data Loading ---
    cout << endl << "--- Data ---" << endl;
    auto mnist_data = MnistData::LoadData("train-images-idx3-ubyte");
    auto mnist_label = MnistData::LoadLabel("train-labels-idx1-ubyte");
    auto val_data = MnistData::LoadData("t10k-images-idx3-ubyte");
    auto val_label = MnistData::LoadLabel("t10k-labels-idx1-ubyte");
    const unsigned int data_size = mnist_data.size();

    if(!data_size)
    {
        cout << "No data loaded" << endl;
        return 0;
    }

    const unsigned int in_dimension = mnist_data[0]->cols();
    const unsigned int out_classes = mnist_label[0]->cols();

    auto data_split = Utils::SplitData(mnist_data, mnist_label);
    auto train_split = get<0>(data_split);
    auto test_split = get<1>(data_split);
    auto val_split = Utils::BundleDataLabel(val_data, val_label);
    const unsigned int train_size = train_split.size();
    const unsigned int test_size = test_split.size();
    const unsigned int val_size = val_split.size();

    cout << "Data : " << data_size + val_size << " samples -> "
        << train_size << " train + "
        << test_size << " test + "
        << val_size << " validation" << endl;

    // --- Network ---
    cout << endl << "--- Network ---" << endl;
    auto symbolic_input = make_shared<Placeholder>(1, in_dimension);
    auto fc1 = FCLayer(symbolic_input, in_dimension, 100, Activation::Sigmoid);
    auto fc2 = FCLayer(fc1.output, 100, out_classes, Activation::Sigmoid);
    auto network_output = fc2.output;

    NetworkFeed feed = {{symbolic_input.get(), nullptr}};

    Utils::PrintGraph(network_output);

    // --- Test ---
    cout << endl << "--- Test ---" << endl;
    cout << "Test accuracy : " << Utils::TestAccuracy(test_split, feed, symbolic_input, network_output) << "%" << endl;

    // --- Train ---
    cout << endl << "--- Train ---" << endl;
    auto loss_gradient = MatrixXf(1, out_classes);
    unsigned int batch_step = 0;
    for(unsigned int epoch = 0 ; epoch < max_epoch ; epoch += 1)
    {
        for(auto& data_tuple: train_split)
        {
            // Infer and calculate loss gradient (using derivate of mean square wich is just a substraction here)
            feed[symbolic_input.get()] = get<0>(data_tuple);
            loss_gradient.noalias() = network_output->Infer(feed) - *(get<1>(data_tuple));
            // Calculate gradients
            network_output->BackProp(loss_gradient);
            // Update variables
            batch_step += 1;
            if(batch_size == 1)
            {
                network_output->ApplyGradient(learning_rate);
            }
            else
            {
                network_output->AccumulateGradient(learning_rate, batch_size);
                if(batch_step >= (batch_size - 1))
                {
                    network_output->ApplyAccumulatedGradient();
                    batch_step = 0;
                }
            }
        }

        if((epoch % (unsigned int)(max_epoch / max_train_output)) == 0)
            cout << "End of epoch " << epoch
                << " : test accuracy " << Utils::TestAccuracy(test_split, feed, symbolic_input, network_output) << "%" << endl;
    }
    cout << "Training over" << endl;
    cout << "End of epoch " << max_epoch - 1
            << " : test accuracy " << Utils::TestAccuracy(test_split, feed, symbolic_input, network_output) << "%" << endl;

    // --- Test ---
    cout << endl << "--- Final test ---" << endl;
    cout << "Test accuracy : " << Utils::TestAccuracy(test_split, feed, symbolic_input, network_output) << "%" << endl;

    // --- Validation ---
    cout << endl << "--- Validation ---" << endl;
    cout << "Test accuracy : " << Utils::TestAccuracy(val_split, feed, symbolic_input, network_output) << "%" << endl;

    cout << "Done" << endl;
    return 0;
}

