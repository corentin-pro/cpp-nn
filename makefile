CC := g++

SOURCE_DIR := src
OBJECT_DIR := object
OBJECT_PROD_DIR := object/prod
OBJECT_DEBUG_DIR := object/debug
INCLUDE_DIR := include

COMMON_FLAGS = -std=c++14 -fopenmp
LINK_FLAGS := $(COMMON_FLAGS)
COMPILE_FLAGS := $(COMMON_FLAGS) -Wall `pkg-config --cflags "eigen3"` -I$(INCLUDE_DIR)

CPP_LIB_SOURCES := $(wildcard $(SOURCE_DIR)/lib/*.cpp)
CPP_IRIS_SOURCES := src/iris.cpp ${CPP_LIB_SOURCES}
CPP_IRIS_GRAPH_SOURCES := src/iris_graph.cpp ${CPP_LIB_SOURCES}
CPP_MNIST_SOURCES := src/mnist.cpp ${CPP_LIB_SOURCES}
CPP_SOURCES := $(wildcard $(SOURCE_DIR)/*.cpp) ${CPP_LIB_SOURCES}

CPP_PROD_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_PROD_DIR)/%, ${CPP_SOURCES:.cpp=.o})
CPP_DEBUG_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_DEBUG_DIR)/%, ${CPP_SOURCES:.cpp=.o})
CPP_IRIS_PROD_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_PROD_DIR)/%, ${CPP_IRIS_SOURCES:.cpp=.o})
CPP_IRIS_DEBUG_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_DEBUG_DIR)/%, ${CPP_IRIS_SOURCES:.cpp=.o})
CPP_IRIS_GRAPH_PROD_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_PROD_DIR)/%, ${CPP_IRIS_GRAPH_SOURCES:.cpp=.o})
CPP_IRIS_GRAPH_DEBUG_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_DEBUG_DIR)/%, ${CPP_IRIS_GRAPH_SOURCES:.cpp=.o})
CPP_MNIST_PROD_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_PROD_DIR)/%, ${CPP_MNIST_SOURCES:.cpp=.o})
CPP_MNIST_DEBUG_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_DEBUG_DIR)/%, ${CPP_MNIST_SOURCES:.cpp=.o})


# Extra options
.PHONY: all debug prod clean
.SILENT: clean

# Default build
all: prod

# Main builds
prod: COMPILE_FLAGS += -O2
prod: LINK_FLAGS += -O2
prod: build

debug: COMPILE_FLAGS += -DDEBUG -g
debug: LINK_FLAGS += -DDEBUG -g
debug: build_debug


# Final link
build: $(CPP_PROD_OBJECTS)
	@echo -e ""
	@echo -e "\033[1m\033[92m### Linking application\033[0m"
	$(CC) $(CPP_IRIS_PROD_OBJECTS) -o iris $(LINK_FLAGS)
	$(CC) $(CPP_IRIS_GRAPH_PROD_OBJECTS) -o iris_graph $(LINK_FLAGS)
	$(CC) $(CPP_MNIST_PROD_OBJECTS) -o mnist $(LINK_FLAGS)
	@echo -e ""
	@echo -e ""

build_debug: $(CPP_DEBUG_OBJECTS)
	@echo -e ""
	@echo -e "\033[1m\033[92m### Linking application in DEBUG mode\033[0m"
	$(CC) $(CPP_IRIS_DEBUG_OBJECTS) -o iris_debug $(LINK_FLAGS)
	$(CC) $(CPP_IRIS_GRAPH_DEBUG_OBJECTS) -o iris_graph_debug $(LINK_FLAGS)
	$(CC) $(CPP_MNIST_DEBUG_OBJECTS) -o mnist_debug $(LINK_FLAGS)
	@echo -e ""

# Compile
$(OBJECT_PROD_DIR)/%.o: $(SOURCE_DIR)/%.cpp
	@echo -e "\033[92m### Compiling $<\033[0m"
	@mkdir -p $(dir $@)
	$(CC) $(COMPILE_FLAGS) $< -c -o $@

$(OBJECT_DEBUG_DIR)/%.o: $(SOURCE_DIR)/%.cpp
	@echo -e "\033[92m### Compiling $< in DEBUG mode\033[0m"
	@mkdir -p $(dir $@)
	$(CC) $(COMPILE_FLAGS) $< -c -o $@


# Mandatory clean
clean:
	@rm -rf $(OBJECT_DIR)
	@rm -f iris
	@rm -f iris_graph
	@rm -f mnist
	@rm -f *_debug
	@rm -f version
	@echo -e "\033[1m\033[93m### Project cleaned $<\033[0m"
	@echo -e ""
