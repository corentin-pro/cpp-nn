
mnist_files="train-images-idx3-ubyte.gz \
train-labels-idx1-ubyte.gz \
t10k-images-idx3-ubyte.gz \
t10k-labels-idx1-ubyte.gz"

for file in $mnist_files
do
    wget -N "http://yann.lecun.com/exdb/mnist/$file"
    gunzip -k $file
done
